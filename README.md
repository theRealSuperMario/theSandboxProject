[![pipeline status](https://gitlab.com/theRealSuperMario/theSandboxProject/badges/master/pipeline.svg)](https://gitlab.com/theRealSuperMario/theSandboxProject/commits/master)

[![coverage report](https://gitlab.com/theRealSuperMario/theSandboxProject/badges/master/coverage.svg)](https://gitlab.com/theRealSuperMario/theSandboxProject/commits/master)


conda env:
 
sandbox_36



* run tests using tox
* remember to add test coverage regex in pipeline settings
* test coverage parsing
